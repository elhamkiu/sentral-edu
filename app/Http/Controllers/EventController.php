<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Event;
use App\Models\Category;
use App\Models\Organiser;
use App\Models\School;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::all();

        return view('events.index', compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $userInfo = $this->userInfo();
        return view('events.create',compact('userInfo'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
     {
  
        $request->validate([
            'title'=>'required',
            'category_id'=>'required',
            'description'=>'required',
            'school_id'=>'required'
        ]);

        $addressTo = $request->get('address');
        $addressFrom = $this->getSchoolAddress($request->get('school_id'));
        $mapInfo = $this-> getDistance($addressFrom, $addressTo, $unit = 'K');

        $event = new Event([
            'title' => $request->get('title'),
            'category_id' => $request->get('category_id'),
            'description' => $request->get('description'),
            'school_id' => $request->get('school_id'),
            'date' => $request->get('date'),
            'time' => $request->get('time'),
            'address' => $request->get('address'),
            'organiser_id' => $request->get('organiser_id'),
            'distance' => $mapInfo['distance'],
            'travel_time' => $mapInfo['travel_time']
        ]);

        $event->save();
        return redirect('/events')->with('success', 'Event saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::find($id);
        $userInfo = $this->userInfo();
        return view('events.edit', compact('event','userInfo'));       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title'=>'required',
            'category_id'=>'required',
            'description'=>'required',
            'school_id'=>'required'
        ]);

        $event = Event::find($id);
        $event->title =  $request->get('title');
        $event->category_id = $request->get('category_id');
        $event->description = $request->get('description');
        $event->school_id = $request->get('school_id');
        $event->save();

        return redirect('/events')->with('success', 'Event updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::find($id);
        $event->delete();
        return redirect('/events')->with('success', 'Event deleted!');
    }

    private function userInfo(){
        
        $userInfo['categories'] = Category::all();
        $userInfo['orgnisers'] = Organiser::all();
        $userInfo['schools'] = School::all();
        return $userInfo;
    }

    /**
 * @function getDistance()
 * Calculates the distance between two address
 * 
 * @params
 * $addressFrom - Starting point
 * $addressTo - End point
 * $unit - Unit type
 * 
 * @author CodexWorld
 * @url https://www.codexworld.com
 *
 */
function getDistance($addressFrom, $addressTo, $unit = ''){
     
    // Google API key
    $apiKey = 'AIzaSyDdzSlZxREsZwk8H70WufatM2UT3T9Vlo4';
    
    // Change address format
    $formattedAddrFrom    = str_replace(' ', '+', $addressFrom);
    $formattedAddrTo     = str_replace(' ', '+', $addressTo);
    
    // Geocoding API request with start address
    $geocodeFrom = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddrFrom.'&sensor=false&key='.$apiKey);
    $outputFrom = json_decode($geocodeFrom);
    if(!empty($outputFrom->error_message)){
        return $outputFrom->error_message;
    }
    
    // Geocoding API request with end address
    $geocodeTo = file_get_contents('https://maps.googleapis.com/maps/api/geocode/json?address='.$formattedAddrTo.'&sensor=false&key='.$apiKey);
    $outputTo = json_decode($geocodeTo);
    if(!empty($outputTo->error_message)){
        return $outputTo->error_message;
    }

    // Get latitude and longitude from the geodata
    $latitudeFrom    = $outputFrom->results[0]->geometry->location->lat;
    $longitudeFrom    = $outputFrom->results[0]->geometry->location->lng;
    $latitudeTo        = $outputTo->results[0]->geometry->location->lat;
    $longitudeTo    = $outputTo->results[0]->geometry->location->lng;
    
    // Calculate distance between latitude and longitude
    $theta    = $longitudeFrom - $longitudeTo;
    $dist    = sin(deg2rad($latitudeFrom)) * sin(deg2rad($latitudeTo)) +  cos(deg2rad($latitudeFrom)) * cos(deg2rad($latitudeTo)) * cos(deg2rad($theta));
    $dist    = acos($dist);
    $dist    = rad2deg($dist);
    $miles    = $dist * 60 * 1.1515;

    $mapInfo['distance'] = round($miles * 1.609344, 2).' km';
    
    //travel time 
    
    $data = file_get_contents('https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins='.$formattedAddrFrom.'&destinations='.$formattedAddrTo.'&key='.$apiKey);
    $data = json_decode($data,true);
    $mapInfo['travel_time']  = $data['rows'][0]['elements'][0]['duration']['text'];

    return $mapInfo;
 }


 private function getSchoolAddress($school_id){

    $school = School::find($school_id);
    return $school->address;
 }

}
