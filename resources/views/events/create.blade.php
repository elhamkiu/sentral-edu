@extends('base')

@section('main')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Add an Event</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('events.store') }}">
          @csrf
          <div class="form-group">    
              <label for="title">Title:</label>
              <input type="text" class="form-control" name="title"/>
          </div>

          <div class="form-group">
              <label for="description">Description:</label>
              <input type="text" class="form-control" name="description"/>
          </div>

          <div class="form-group">
              <label for="address">Address:</label>
              <input type="text" class="form-control" name="address"/>
          </div>

          <div class="form-group"> <!-- Date input -->
            <label class="control-label" for="date">Date</label>
            <input class="form-control" id="date" name="date" placeholder="MM/DD/YYY" type="date"/>
          </div>

          <div class="form-group"> <!-- Date input -->
            <label class="control-label" for="time">Time</label>
            <input class="form-control" id="time" name="time"  type="time"/>
          </div>

          <div class="form-group">
              <label for="category">Category:</label>
              <select class="form-control" name="category_id">
                @foreach($userInfo['categories'] as $category)
                <option value={{$category->id}}>{{$category->title}}</option>
                @endforeach
              </select>
          </div>

          <div class="form-group">
              <label for="organiser">Orgniser:</label>
              <select class="form-control" name="organiser_id">
                @foreach($userInfo['orgnisers'] as $organiser)
                <option value={{$organiser->id}}>{{$organiser->title}}</option>
                @endforeach
              </select>
          </div>

          <div class="form-group">
              <label for="school">Shool::</label>
              <select class="form-control" name="school_id">
                @foreach($userInfo['schools'] as $school)
                <option value={{$school->id}}><b>{{$school->name}}</b> : {{$school->address}}</option>
                @endforeach
              </select>
          </div>

        
                                  
          <button type="submit" class="btn btn-primary">Add Event</button>
      </form>
  </div>
</div>
</div>

@endsection

