@extends('base') 
@section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update a event</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        <form method="post" action="{{ route('events.update', $event->id) }}">
            @method('PATCH') 
            @csrf
            <div class="form-group">    
              <label for="title">Title:</label>
              <input type="text" class="form-control" name="title" value="{{$event->title}}"/>
          </div>

          <div class="form-group">
              <label for="description">Description:</label>
              <input type="text" class="form-control" name="description" value="{{$event->description}}"/>
          </div>

          <div class="form-group">
              <label for="address">Address:</label>
              <input type="text" class="form-control" name="address" value="{{$event->address}}"/>
          </div>

          <div class="form-group"> <!-- Date input -->
            <label class="control-label" for="date">Date</label>
            <input class="form-control" id="date" name="date" placeholder="MM/DD/YYY" type="date" value="{{$event->date}}"/>
          </div>

          <div class="form-group"> <!-- Time input -->
            <label class="control-label" for="time">Time</label>
            <input class="form-control" id="time" name="time"  type="time" value="{{$event->time}}"/>
          </div>

          <div class="form-group">
              <label for="category">Category: </label>
              <select class="form-control" name="category_id">
                @foreach($userInfo['categories'] as $category)
                <option value="{{$category->id}}" {{ $event->category_id == $category->id ? 'selected' : '' }}>{{$category->title}}</option>
                @endforeach
              </select>
          </div>

          <div class="form-group">
              <label for="organiser">Orgniser:</label>
              <select class="form-control" name="organiser_id">
                @foreach($userInfo['orgnisers'] as $organiser)
                <option value="{{$organiser->id}}" {{ $event->organiser_id == $organiser->id ? 'selected' : '' }}>{{$organiser->title}}</option>
                @endforeach
              </select>
          </div>

          <div class="form-group">
              <label for="school">Shool::</label>
              <select class="form-control" name="school_id">
                @foreach($userInfo['schools'] as $school)
                <option value={{$school->id}} {{ $event->school_id == $school->id ? 'selected' : '' }}><b>{{$school->name}}</b> : {{$school->address}}</option>
                @endforeach
              </select>
          </div>
      
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
@endsection