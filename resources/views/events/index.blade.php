@extends('base')

@section('main')
<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
<div>
    <a style="margin: 19px;" href="{{ route('events.create')}}" class="btn btn-primary">New event</a>
    </div>  
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Events</h1>    
  <table class="table table-striped">
    <thead>
        <tr>
          <td>ID</td>
          <td>Title</td>
          <td>Category</td>
          <td>Description</td>
          <td>School</td>
          <td>Distance</td>
          <td>Travel Time</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($events as $event)
        <tr>
            <td>{{$event->id}}</td>
            <td>{{$event->title}} </td>
            <td>{{$event->category_id}}</td>
            <td>{{$event->description}}</td>
            <td>{{$event->school_id}}</td>
            <td>{{$event->distance}}</td>
            <td>{{$event->travel_time}}</td>
            <td>
                <a href="{{ route('events.edit',$event->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <form action="{{ route('events.destroy', $event->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
</div>
@endsection

