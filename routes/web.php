<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
GET/events, mapped to the index() method,
GET /events/create, mapped to the create() method,
POST /events, mapped to the store() method,
GET /events/{event}, mapped to the show() method,
GET /events/{event}/edit, mapped to the edit() method,
PUT/PATCH /events/{event}, mapped to the update() method,
DELETE /events/{event}, mapped to the destroy() method.
*/
Route::resource('events', 'EventController');
